/*
 * This file is part of Facere.
 *
 * Facere is free software: you can redistribute it and/or modify
 * it under the terms of version 3 of the GNU General Public License as
 * published by Free Software Foundation.
 *
 * Facere is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 3
 * of the GNU General Public License for more details.
 *
 * You should have received a copy of version 3 of the GNU General Public
 * License along with Facere.  If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * To contact the author of Facere, send an email to
 * <seblavine@outlook.com>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "link.h"
#include "texts.h" // license_info, help_info strings
  
int main(int argc, char **argv) {
  if (argc == 1)
    
    puts(license_info);
  else
    for (int i = 1; i < argc; i++)
      if (strcmp(*(argv + i), "help") == 0)
	puts(help_info);
      else if (strcmp(*(argv + i), "info") == 0)
	puts(license_info);
      else if (strcmp(*(argv + i), "version") == 0)
	printf("Time of compilation: %s %s\n", __DATE__, __TIME__);
      else if (strcmp(*(argv + i), "list") == 0
	       || strcmp(*(argv + i), "print") == 0)
	print_to_dos();
      else if (strcmp(*(argv + i), "new") == 0       
	       || strcmp(*(argv + i), "create") == 0
	       || strcmp(*(argv + i), "add") == 0) {
	add_to_do(i, argc, argv);
	break;
      }
      else
	printf("Unknown argument \"%s\".\n", *(argv + i));
  return 0;
}
